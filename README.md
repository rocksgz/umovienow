# **UMovieNow** #
Repositorio entregable para aplicar a oportunidad en Grability

## **Inicio** ##

![Scheme](screenshots/popular.png)

## **Peliculas mas votadas** ##

![Scheme](screenshots/toprated.png)

## **Barra de búsqueda** ##

![Scheme](screenshots/searchbar.png)

## **Detalle de la Pelicula** ##

![Scheme](screenshots/moviedetail.png)

## **Resultados de búsqueda** ##

![Scheme](screenshots/searchresults.png)